import tkinter as tk
from tkinter import messagebox

class TicTacToe:
    def __init__(self, root):
        self.buttons = []
        for i in range(3):
            button_row = []
            for j in range(3):
                button = tk.Button(root, width=10, height=5, command=lambda row=i, col=j: self.play(row, col))
                button.grid(row=i, column=j)
                button_row.append(button)
            self.buttons.append(button_row)

        self.player = 'X'
        self.game_over = False

    def play(self, row, col):
        if self.game_over:
            return

        if self.buttons[row][col]['text'] != '':
            return

        self.buttons[row][col]['text'] = self.player

        if self.check_win():
            self.game_over = True
            tk.messagebox.showinfo('Fin de la partie', f'Le joueur {self.player} a gagné !')

        if self.player == 'X':
            self.player = 'O'
        else:
            self.player = 'X'

    def check_win(self):
        for row in self.buttons:
            if row[0]['text'] == row[1]['text'] == row[2]['text'] and row[0]['text'] != '':
                return True

        for col in range(3):
            if self.buttons[0][col]['text'] == self.buttons[1][col]['text'] == self.buttons[2][col]['text'] and self.buttons[0][col]['text'] != '':
                return True

        if self.buttons[0][0]['text'] == self.buttons[1][1]['text'] == self.buttons[2][2]['text'] and self.buttons[0][0]['text'] != '':
            return True
        if self.buttons[0][2]['text'] == self.buttons[1][1]['text'] == self.buttons[2][0]['text'] and self.buttons[0][2]['text'] != '':
            return True

        return False

root = tk.Tk()
root.title('Morpion')

game = TicTacToe(root)

root.mainloop()

